package log

import (
	"fmt"
	"testing"

	"gitlab.com/flimzy/testy"
)

type testLog struct {
	Logger
	logs []string
}

func (l *testLog) append(label, format string, args ...interface{}) {
	l.logs = append(l.logs,
		fmt.Sprintf("["+label+"] "+format, args...))
}

func (l *testLog) Debugf(format string, args ...interface{}) {
	l.append("DEBUG", format, args...)
}

func (l *testLog) Printf(format string, args ...interface{}) {
	l.append("INFO", format, args...)
}

func (l *testLog) Errorf(format string, args ...interface{}) {
	l.append("ERROR", format, args...)
}

func TestWriter(t *testing.T) {
	lg := &testLog{}
	debug := DebugWriter(lg)
	info := InfoWriter(lg)
	err := ErrorWriter(lg)
	fmt.Fprintf(debug, "Moo")
	fmt.Fprintf(info, "Quack")
	fmt.Fprintf(err, "Oink")
	fmt.Fprintf(info, "Test with formatting verbs %s", "%s")
	expected := []string{
		"[DEBUG] Moo",
		"[INFO] Quack",
		"[ERROR] Oink",
		"[INFO] Test with formatting verbs %s",
	}

	if d := testy.DiffTextSlices(expected, lg.logs); d != nil {
		t.Error(d)
	}
}
