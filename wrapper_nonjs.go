// +build !js

package log

import "gitlab.com/FlashbackSRS/log/sentry/errors"

// WithError adds err, and any embedded fields, to the entry.
func (w *Wrapper) WithError(err error) Logger {
	lg := w.Logger
	if fields := ErrFields(errors.RavenError(err)); fields != nil {
		lg = lg.WithFields(fields)
	}
	lg = lg.WithError(err)
	return &Wrapper{lg}
}
