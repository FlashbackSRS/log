package testlog

import (
	"fmt"
	"net/http"
	"sort"
	"strings"
	"sync"
	"testing"

	"gitlab.com/flimzy/testy"

	"gitlab.com/FlashbackSRS/log"
)

// Logger is a test implementation of log.Logger
type Logger struct {
	logs []string
	mu   sync.Mutex
}

// New returns a new test logger. This logger ignores fields.
func New() *Logger {
	return &Logger{
		logs: make([]string, 0),
	}
}

var _ log.Logger = &Logger{}

func (l *Logger) printf(prefix string, format string, args ...interface{}) {
	l.mu.Lock()
	defer l.mu.Unlock()
	l.logs = append(l.logs, fmt.Sprintf(prefix+" "+format, args...))
}

// Debugf records a debug message.
func (l *Logger) Debugf(format string, args ...interface{}) {
	l.printf("[DEBUG]", format, args...)
}

// Errorf records an error message.
func (l *Logger) Errorf(format string, args ...interface{}) {
	l.printf("[ERROR]", format, args...)
}

// Printf records an informational message.
func (l *Logger) Printf(format string, args ...interface{}) {
	l.printf("[INFO]", format, args...)
}

func (l *Logger) Log(level log.Level, msg string) {
	l.printf(fmt.Sprintf("[%s]", strings.ToUpper(level.String())), msg)
}

// Flush waits for all concurrent recordings to finish.
func (l *Logger) Flush() error {
	l.mu.Lock()
	defer l.mu.Unlock()
	return nil
}

// WithFields logs the Fields passed, and returns an unaltered l.
func (l *Logger) WithFields(fields log.Fields) log.Logger {
	keys := make([]string, 0, len(fields))
	for k := range fields {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	for _, k := range keys {
		val := fields[k]
		switch t := val.(type) {
		case *http.Request:
			val = fmt.Sprintf("*http.Request: %s %s", t.Method, t.URL)
		}
		l.printf("[FIELD]", "%s=%+q", k, val)
	}
	return l
}

// WithError logs the error passed to log.FieldError key.
func (l *Logger) WithError(err error) log.Logger {
	return l.WithFields(log.Fields{
		log.FieldError: err,
	})
}

// Err uses the error's text as message.
func (l *Logger) Err(err error) {
	l.WithError(err).Errorf("%v", err)
}

// Logs returns all accumulated logs.
func (l *Logger) Logs() []string {
	l.mu.Lock()
	defer l.mu.Unlock()
	return l.logs
}

// IsLevelEnabled returns true.
func (l *Logger) IsLevelEnabled(_ log.Level) bool {
	return true
}

// Check compares the actual logs recorded against expectations.
func (l *Logger) Check(t *testing.T, res ...testy.Replacement) {
	l.CheckN(t, 0, res...)
}

// CheckN compares the first N actual logs recorded against expectations.
func (l *Logger) CheckN(t *testing.T, n int, res ...testy.Replacement) {
	t.Helper()
	l.mu.Lock()
	defer l.mu.Unlock()
	logs := l.logs
	if n > 0 && len(logs) > n {
		logs = logs[:n]
	}
	r := strings.NewReader(strings.Join(logs, "\n"))
	if d := testy.DiffText(&testy.File{Path: "testdata/" + testy.Stub(t) + ".log"}, r, res...); d != nil {
		t.Errorf("Unexpected log output:\n%s\n", d)
	}
}
