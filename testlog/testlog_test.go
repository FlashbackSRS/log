package testlog

import "testing"

func TestCheckN(t *testing.T) {
	l := &Logger{
		logs: []string{"one", "two", "three"},
	}
	l.CheckN(t, 2)
}
