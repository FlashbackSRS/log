module gitlab.com/FlashbackSRS/log

go 1.12

require (
	github.com/certifi/gocertifi v0.0.0-20200211180108-c7c1fbc02894 // indirect
	github.com/evalphobia/logrus_sentry v0.8.2
	github.com/getsentry/raven-go v0.2.0
	github.com/google/go-cmp v0.4.0
	github.com/gopherjs/gopherjs v0.0.0-20190812055157-5d271430af9f
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/otiai10/copy v1.1.1 // indirect
	github.com/pkg/errors v0.9.1
	github.com/sebest/logrusly v0.0.0-20180315190218-3235eccb8edc
	github.com/segmentio/go-loggly v0.5.0 // indirect
	github.com/sirupsen/logrus v1.5.0
	gitlab.com/flimzy/ale v0.0.1
	gitlab.com/flimzy/testy v0.1.1
	golang.org/x/sys v0.0.0-20200409092240-59c9f1ba88fa // indirect
	golang.org/x/xerrors v0.0.0-20191204190536-9bdfabe68543
)
