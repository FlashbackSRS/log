package log

import (
	"context"
)

type contextKey struct {
	name string
}

var (
	loggerKey = &contextKey{"logger"}
)

// SetLogger sets the context's logger.
func SetLogger(ctx context.Context, log Logger) context.Context {
	return context.WithValue(ctx, loggerKey, log)
}

// GetLogger returns the context's logger, or nil if none.
func GetLogger(ctx context.Context) Logger {
	v, _ := ctx.Value(loggerKey).(Logger)
	return v
}
