package log

import (
	"fmt"
	"net/http"
	"sort"
	"strings"
	"sync"
	"testing"
	"time"

	"gitlab.com/flimzy/ale"
	"gitlab.com/flimzy/testy"
)

func TestParseIP(t *testing.T) {
	tests := []struct {
		name     string
		input    string
		expected string
	}{
		{
			name:     "IPv4",
			input:    "1.2.3.4:988",
			expected: "1.2.3.4",
		},
		{
			name:     "Unknown",
			input:    "",
			expected: "<unknown>",
		},
		{
			name:     "IPv6",
			input:    "[2001:db8::1]:8999",
			expected: "[2001:db8::1]",
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result := parseIP(test.input)
			if result != test.expected {
				t.Errorf("Unexpected result. Expected %q, got %q", test.expected, result)
			}
		})
	}
}

func TestMSMarshalJSON(t *testing.T) {
	tests := []struct {
		name     string
		input    time.Duration
		expected string
	}{
		{
			name:     "zero",
			input:    0,
			expected: "0",
		},
		{
			name:     "5 seconds",
			input:    5 * time.Second,
			expected: "5000",
		},
		{
			name:     "5 ns",
			input:    5 * time.Nanosecond,
			expected: "0.000005",
		},
		{
			name:     "5 ms",
			input:    5 * time.Millisecond,
			expected: "5",
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			m := ms(test.input)
			result, _ := m.MarshalJSON()
			if string(result) != test.expected {
				t.Errorf("Unexpected result. Expected %q, got %q", test.expected, string(result))
			}
		})
	}
}

type testlogger struct {
	Logger
	mu   *sync.Mutex
	logs []string
}

func newTestLogger() *testlogger {
	return &testlogger{
		logs: make([]string, 0),
		mu:   new(sync.Mutex),
	}
}

func (l *testlogger) WithFields(fields Fields) Logger {
	keys := make([]string, 0, len(fields))
	for k := range fields {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	for _, k := range keys {
		l.printf("[FIELD]", "%s=%+q", k, fields[k])
	}
	return l
}

func (l *testlogger) printf(prefix string, format string, args ...interface{}) {
	l.mu.Lock()
	defer l.mu.Unlock()
	l.logs = append(l.logs, fmt.Sprintf(prefix+" "+format, args...))
}

func (l *testlogger) Printf(format string, args ...interface{}) {
	l.printf("[INFO]", format, args...)
}

func (l *testlogger) Errorf(format string, args ...interface{}) {
	l.printf("[ERROR]", format, args...)
}

func (l *testlogger) Check(t *testing.T) {
	l.mu.Lock()
	defer l.mu.Unlock()
	r := strings.NewReader(strings.Join(l.logs, "\n"))

	if d := testy.DiffText(&testy.File{Path: "testdata/" + testy.Stub(t) + ".log"}, r); d != nil {
		t.Errorf("Unexpected log output:\n%s\n", d)
	}
}

func TestRequest(t *testing.T) {
	tests := testy.NewTable()
	tests.Add("get", &ale.LogContext{
		Request: &http.Request{
			Method:     http.MethodGet,
			RequestURI: "http://example.com/",
			RemoteAddr: "192.168.0.1:9876",
		},
		StatusCode:    http.StatusOK,
		ResponseBytes: 200,
		ElapsedTime:   15 * time.Millisecond,
	})
	tests.Add("post", &ale.LogContext{
		Request: &http.Request{
			Method:     http.MethodPost,
			RequestURI: "http://example.com/",
			RemoteAddr: "192.168.0.1:9876",
		},
		StatusCode:    http.StatusOK,
		ResponseBytes: 200,
		ElapsedTime:   15 * time.Millisecond,
	})
	tests.Add("authenticated", &ale.LogContext{
		Request: &http.Request{
			Method:     http.MethodGet,
			RequestURI: "http://example.com/",
			RemoteAddr: "192.168.0.1:9876",
		},
		StatusCode:    http.StatusOK,
		ResponseBytes: 200,
		ElapsedTime:   15 * time.Millisecond,
		AuthUser:      "bob",
	})
	tests.Add("error", &ale.LogContext{
		Request: &http.Request{
			Method:     http.MethodGet,
			RequestURI: "http://example.com/",
			RemoteAddr: "192.168.0.1:9876",
		},
		StatusCode:    http.StatusInternalServerError,
		ResponseBytes: 200,
		ElapsedTime:   15 * time.Millisecond,
		AuthUser:      "bob",
	})
	tests.Add("user agent", &ale.LogContext{
		Request: &http.Request{
			Method:     http.MethodGet,
			RequestURI: "http://example.com/",
			RemoteAddr: "192.168.0.1:9876",
			Header: http.Header{
				"User-Agent": []string{"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.75 Safari/537.36"},
			},
		},
		StatusCode:    http.StatusOK,
		ResponseBytes: 200,
		ElapsedTime:   15 * time.Millisecond,
		AuthUser:      "bob",
	})
	tests.Add("redirect", &ale.LogContext{
		Request: &http.Request{
			Method:     http.MethodGet,
			RequestURI: "http://example.com/",
			RemoteAddr: "192.168.0.1:9876",
		},
		ResponseHeader: http.Header{
			"Location": []string{"https://example.com/"},
		},
		StatusCode:    http.StatusFound,
		ResponseBytes: 200,
		ElapsedTime:   15 * time.Millisecond,
		AuthUser:      "bob",
	})
	tests.Run(t, func(t *testing.T, ctx *ale.LogContext) {
		lg := newTestLogger()
		Request(lg, ctx)
		lg.Check(t)
	})
}
