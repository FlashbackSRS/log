// +build !js

package errors

import (
	"errors"
	"go/build"
	"regexp"
	"runtime"
	"testing"

	"github.com/evalphobia/logrus_sentry"
	raven "github.com/getsentry/raven-go"
	pkgerrors "github.com/pkg/errors"
	"gitlab.com/flimzy/testy"
)

type rerr struct {
	error
	stack *raven.Stacktrace
}

func (e *rerr) GetStacktrace() *raven.Stacktrace {
	return e.stack
}

func TestRavenError(t *testing.T) {
	type tt struct {
		err error
	}
	tests := testy.NewTable()
	tests.Add("no stack", tt{
		err: errors.New("foo"),
	})
	tests.Add("raven stack", func(t *testing.T) interface{} {
		err := errors.New("foo")
		return tt{
			err: &rerr{
				error: err,
				stack: raven.GetOrNewStacktrace(err, 0, 0, []string{"gitlab.com/FlashbackSRS/log"}),
			},
		}
	})
	tests.Add("pkg/errors stack", tt{
		err: pkgerrors.WithStack(errors.New("foo")),
	})

	re := []testy.Replacement{
		{
			Regexp:      regexp.MustCompile(`\(len=\d+\) ".*/vendor/`),
			Replacement: `(len=XX) "`,
		},
		{
			Regexp:      regexp.MustCompile(`: \(string\) \(len=\d+\) ".*/FlashbackSRS/log`),
			Replacement: `: (string) (len=XX) ".../FlashbackSRS/log`,
		},
		{
			Regexp:      regexp.MustCompile(`: \(string\) \(len=\d+\) "` + runtime.GOROOT()),
			Replacement: `: (string) (len=XX) "...`,
		},
	}
	if gopath := build.Default.GOPATH; gopath != "" {
		re = append(re, testy.Replacement{
			Regexp:      regexp.MustCompile(`: \(string\) \(len=\d+\) "` + gopath + `/pkg/mod`),
			Replacement: `: (string) (len=XX) "...`,
		})
	}
	tests.Run(t, func(t *testing.T, tt tt) {
		err := RavenError(tt.err)
		result := err.(logrus_sentry.Stacktracer).GetStacktrace()
		if d := testy.DiffInterface(testy.Snapshot(t), result, re...); d != nil {
			t.Error(d)
		}
	})
}
