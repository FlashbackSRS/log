package errors

import (
	"github.com/evalphobia/logrus_sentry"
	raven "github.com/getsentry/raven-go"
	"github.com/pkg/errors"
	"golang.org/x/xerrors"
)

type ravenError struct {
	error
}

// RavenError converts err to an error with a Raven-compatible stack trace.
func RavenError(err error) error {
	return &ravenError{err}
}

type stackTracer interface {
	StackTrace() errors.StackTrace
	Error() string
}

type causer interface {
	Cause() error
}

func (e *ravenError) GetStacktrace() *raven.Stacktrace {
	err := e.error
	var ravenErr logrus_sentry.Stacktracer
	var stackErr stackTracer
	for {
		if xerrors.As(err, &ravenErr) {
			return ravenErr.GetStacktrace()
		}
		if xerrors.As(err, &stackErr) {
			return raven.GetOrNewStacktrace(err, 0, 1, []string{"gitlab.com/FlashbackSRS"})
		}
		if c, ok := err.(causer); ok {
			err = c.Cause()
			continue
		}
		return nil
	}
}
