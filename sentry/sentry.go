package sentry

import (
	"sync"
	"time"

	"github.com/evalphobia/logrus_sentry"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

// Duplicated from log package, to avoid import loops
const (
	FieldError       = "error"
	FieldStack       = "stack"
	FieldRelease     = "release"
	FieldEnvironment = "environment"
)

// Hook wraps a logrus_sentry.SentryHook
type Hook struct {
	*logrus_sentry.SentryHook
	mu       sync.Mutex
	env, rel string
	fallback func(error) error
}

// NewHook returns a new Sentry hook to be added to an instance of
// log.Logger.
func NewHook(dsn, env, rel string, enableStackTraces bool) (*Hook, error) {
	hook, err := logrus_sentry.NewAsyncSentryHook(dsn, []logrus.Level{
		logrus.PanicLevel,
		logrus.FatalLevel,
		logrus.ErrorLevel,
	})
	if err != nil {
		return nil, err
	}
	hook.Timeout = time.Second
	hook.StacktraceConfiguration = logrus_sentry.StackTraceConfiguration{
		Enable: enableStackTraces,
		InAppPrefixes: []string{
			"gitlab.com/FlashbackSRS",
		},
		Level: logrus.ErrorLevel,
	}
	hook.SetRelease(rel)
	hook.SetEnvironment(env)
	return &Hook{
		SentryHook: hook,
		env:        env,
		rel:        rel,
	}, nil
}

// Fallback sets the fallback logger, used when logging to Sentry fails.
// In case of logginf failure, fn will receive the error, and should
// handle it. If it cannot be handled, it should be returned again,
// which will cause it to be treated by the default logrus behavior.
func (h *Hook) Fallback(fn func(error) error) {
	h.fallback = fn
}

// Fire wraps the normal Fire method, but does some data transformations
// first:
//
// - Normalizes stack traces
func (h *Hook) Fire(entry *logrus.Entry) error {
	if _, ok := entry.Data["skip:sentry"].(bool); ok {
		return nil
	}
	if _, ok := entry.Data[FieldStack].(errors.StackTrace); !ok {
		return h.fire(entry)
	}
	// Make a copy, so changes to Data won't propagate to other hooks
	e := new(logrus.Entry)
	*e = *entry
	e.Data = make(logrus.Fields, len(entry.Data))
	for k, v := range entry.Data {
		if k == FieldStack {
			continue
		}
		e.Data[k] = v
	}
	return h.fire(e)
}

func (h *Hook) fire(entry *logrus.Entry) error {
	h.mu.Lock()
	defer h.mu.Unlock()
	if env, ok := entry.Data[FieldEnvironment].(string); ok {
		if env != h.env {
			h.SentryHook.SetEnvironment(env)
			defer h.SentryHook.SetEnvironment(h.env)
		}
		delete(entry.Data, FieldEnvironment)
	}
	if rel, ok := entry.Data[FieldRelease].(string); ok {
		if rel != h.rel {
			h.SentryHook.SetRelease(rel)
			defer h.SentryHook.SetRelease(h.rel)
		}
		delete(entry.Data, FieldRelease)
	}
	err := h.SentryHook.Fire(entry)
	if err != nil && h.fallback != nil {
		err = h.fallback(err)
	}
	return err
}
