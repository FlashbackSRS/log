package log

import (
	"io"
)

type writer func(string, ...interface{})

var _ io.Writer = writer(func(_ string, _ ...interface{}) {})

func (w writer) Write(p []byte) (int, error) {
	w("%s", string(p))
	return len(p), nil
}

// DebugWriter returns an io.Writer which calls l's Debugf method.
func DebugWriter(l Logger) io.Writer {
	return writer(l.Debugf)
}

// InfoWriter returns an io.Writer which calls l's Printf method.
func InfoWriter(l Logger) io.Writer {
	return writer(l.Printf)
}

// ErrorWriter returns an io.Writer which calls l's Errorf method.
func ErrorWriter(l Logger) io.Writer {
	return writer(l.Errorf)
}
