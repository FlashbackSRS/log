package log

import (
	"time"
)

// Entry is a simple log entry, which can be JSON-marshaled to send from client
// to server.
type Entry struct {
	Level   Level     `json:"level"`
	Time    time.Time `json:"time"`
	Message string    `json:"msg"`
	Data    Fields    `json:"data,omitempty"`
}
