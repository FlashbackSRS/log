package fblogrus

import (
	"gitlab.com/FlashbackSRS/log"
)

func (l *logger) WithError(err error) log.Logger {
	if stack := log.StackTrace(err); stack != nil {
		l = l.WithFields(log.Fields{
			log.FieldStack: stack,
		}).(*logger)
	}
	return l.WithFields(log.Fields{
		log.FieldError: err,
	})
}
