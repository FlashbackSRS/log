package fblogrus

import (
	"errors"
	"os"
	"sync"

	"github.com/sirupsen/logrus"

	"gitlab.com/FlashbackSRS/log"
	"gitlab.com/FlashbackSRS/log/loggly"
	"gitlab.com/FlashbackSRS/log/sentry"
)

// logger wraps a logrus.Logger, to conform to the Logger interface
type logger struct {
	*logrus.Entry
	logglyHook *loggly.Hook
	sentryHook *sentry.Hook
}

var _ log.Logger = &logger{}

func (l *logger) Flush() error {
	var wg sync.WaitGroup
	if l.logglyHook != nil {
		wg.Add(1)
		go func() {
			l.logglyHook.Flush()
			wg.Done()
		}()
	}
	if l.sentryHook != nil {
		wg.Add(1)
		go func() {
			l.sentryHook.Flush()
			wg.Done()
		}()
	}
	wg.Wait()
	return nil
}

func (l *logger) WithFields(fields log.Fields) log.Logger {
	return &logger{
		Entry: l.Entry.WithFields(logrus.Fields(fields)),
	}
}

// Err is the functional equivalent to calling:
//
// l.WithError(err).Errorf("%v", err)
func (l *logger) Err(err error) {
	// This type assertion and redirection is to avoid re-adding the
	// error in the standard Errorf() method.
	l.WithError(err).(*logger).Entry.Errorf("%v", err)
}

func (l *logger) IsLevelEnabled(level log.Level) bool {
	return l.Entry.Logger.IsLevelEnabled(logrus.Level(level))
}

func (l *logger) Errorf(format string, args ...interface{}) {
	if len(args) > 0 {
		err, ok := args[len(args)-1].(error)
		if ok {
			l = l.WithError(err).(*logger)
		}
	}
	l.Entry.Errorf(format, args...)
}

func (l *logger) Log(level log.Level, msg string) {
	l.Entry.Log(logrus.Level(level), msg)
}

// Config is a s list of configurable logging values.
type Config struct {
	LogglyDisabled bool
	LogglyToken    string
	LogglyTags     []string
	SentryDisabled bool
	SentryDSN      string
	Host           string
	Level          log.Level
	Fields         log.Fields
	Release        string
	Environment    string
	NoStackTraces  bool
}

// New instantiates a new Logger interface, which is meant typically to live
// as long as the application.
func New(conf *Config) (log.Logger, error) {
	if conf == nil {
		return nil, errors.New("Must provide config")
	}
	if conf.Level == 0 {
		return nil, errors.New("Must provide log level")
	}
	log := logrus.New()
	log.Out = os.Stderr
	log.Level = logrus.Level(conf.Level)
	log.Formatter = NewFormatter()

	l := &logger{
		Entry: logrus.NewEntry(log),
	}
	if conf.Fields != nil {
		l.Entry = l.Entry.WithFields(logrus.Fields(conf.Fields))
	}
	logglyFallback := func(err error) error {
		return l.errorFallback(err, "loggly")
	}
	sentryFallback := func(err error) error {
		return l.errorFallback(err, "sentry")
	}
	if !conf.LogglyDisabled {
		if err := l.enableLoggly(conf.LogglyToken, conf.Host, conf.Level, conf.NoStackTraces, conf.LogglyTags, logglyFallback); err != nil {
			return nil, err
		}
		l.Debugf("Loggly enabled")
	} else {
		l.Debugf("Loggly disabled by configuration")
	}
	if !conf.SentryDisabled {
		if err := l.enableSentry(conf.SentryDSN, conf.Release, conf.Environment, conf.NoStackTraces, sentryFallback); err != nil {
			return nil, err
		}
		l.Debugf("Sentry enabled")
	} else {
		l.Debugf("Sentry disabled by configuration")
	}
	return l, nil
}

func (l *logger) errorFallback(err error, hook string) error {
	if l.logglyHook == nil || l.sentryHook == nil {
		// Fallback only makes sense if we have more than one hook
		// enabled.
		return err
	}
	go l.WithFields(map[string]interface{}{
		"skip:" + hook: true,
	}).WithError(err).Errorf("Failed to log to %s: %s", hook, err)
	return nil
}

func (l *logger) enableLoggly(token, host string, level log.Level, noStackTraces bool, tags []string, fallback func(error) error) error {
	if token == "" {
		return errors.New("Must provide Loggly token")
	}
	if host == "" {
		return errors.New("Must provide host name")
	}
	l.logglyHook = loggly.NewHook(token, host, logrus.Level(level), !noStackTraces, tags...)
	l.Entry.Logger.Hooks.Add(l.logglyHook)
	l.logglyHook.Fallback(fallback)
	return nil
}

const testSentryDSN = "TESTING"

func (l *logger) enableSentry(dsn, release, environment string, noStackTraces bool, fallback func(error) error) error {
	if dsn == "" {
		return errors.New("Must provide Sentry DSN")
	}
	if release == "" {
		return errors.New("must provide release")
	}
	if environment == "" {
		return errors.New("must provide environment")
	}
	if dsn == testSentryDSN {
		// I hate special cases like this, but using an actual DSN
		// causes network traffic, and creates inconsistent results.
		return nil
	}
	hook, err := sentry.NewHook(dsn, environment, release, !noStackTraces)
	if err != nil {
		return err
	}
	hook.Fallback(fallback)
	l.sentryHook = hook
	l.Entry.Logger.Hooks.Add(hook)
	return nil
}
