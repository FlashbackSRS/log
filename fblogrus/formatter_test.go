// +build !js

package fblogrus

import (
	"regexp"
	"testing"
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/flimzy/testy"
)

func TestFormat(t *testing.T) {
	tests := []struct {
		name  string
		input *logrus.Entry
		err   string
	}{
		{
			name: "success",
			input: &logrus.Entry{
				Time:    time.Time{},
				Data:    map[string]interface{}{"msg": "data msg"},
				Message: "foo",
			},
		},
		{
			name: "proxy",
			input: &logrus.Entry{
				Time: time.Time{},
				Data: map[string]interface{}{
					"msg":             "data msg",
					"clientTimestamp": time.Time{},
				},
				Message: "bar",
			},
		},
		{
			name: "stack trace err",
			input: &logrus.Entry{
				Time: time.Time{},
				Data: map[string]interface{}{
					"error": errors.New("foo"),
				},
				Level:   2,
				Message: "err",
			},
		},
	}
	f := NewFormatter()
	re := testy.Replacement{
		Regexp:      regexp.MustCompile("/.*/FlashbackSRS/"),
		Replacement: ".../FlashbackSRS/",
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result, err := f.Format(test.input)
			testy.Error(t, test.err, err)
			if d := testy.DiffText(testy.Snapshot(t), string(result), re); d != nil {
				t.Error(d)
			}
		})
	}
}
