// +build !js

package fblogrus

import (
	"regexp"
	"testing"

	"gitlab.com/flimzy/testy"

	"gitlab.com/FlashbackSRS/log"
)

func TestNew(t *testing.T) {
	tests := []struct {
		name string
		conf *Config
		err  string
	}{
		{
			name: "No config",
			err:  "Must provide config",
		},
		{
			name: "No loggly token",
			conf: &Config{Level: log.LevelInfo},
			err:  "Must provide Loggly token",
		},
		{
			name: "No host name",
			conf: &Config{Level: log.LevelInfo, LogglyToken: "abc"},
			err:  "Must provide host name",
		},
		{
			name: "No level",
			conf: &Config{LogglyToken: "abc", Host: "foo.com"},
			err:  "Must provide log level",
		},
		{
			name: "Success",
			conf: &Config{
				LogglyToken: "abc",
				SentryDSN:   testSentryDSN,
				Host:        "foo.com",
				Level:       log.LevelInfo,
				Release:     "1.2.3",
				Environment: "test",
			},
		},
		{
			name: "With fields",
			conf: &Config{
				LogglyToken: "abc",
				SentryDSN:   testSentryDSN,
				Host:        "foo.com",
				Level:       log.LevelInfo,
				Fields:      log.Fields{"foo": "bar"},
				Release:     "1.2.3",
				Environment: "test",
			},
		},
		{
			name: "No sentry DSN",
			conf: &Config{
				Level:       log.LevelInfo,
				Host:        "foo.com",
				LogglyToken: "xxx",
			},
			err: "Must provide Sentry DSN",
		},
		{
			name: "Sentry disabled",
			conf: &Config{
				Level:          log.LevelInfo,
				Host:           "foo.com",
				LogglyToken:    "xxx",
				SentryDisabled: true,
			},
		},
		{
			name: "Invalid Sentry DSN",
			conf: &Config{
				Level:       log.LevelInfo,
				Host:        "foo.com",
				LogglyToken: "xxx",
				SentryDSN:   "foo",
				Release:     "1.2.3",
				Environment: "test",
			},
			err: "raven: dsn missing public key and/or password",
		},
		{
			name: "No release",
			conf: &Config{
				Level:       log.LevelInfo,
				Host:        "foo.com",
				LogglyToken: "xxx",
				SentryDSN:   "foo",
				Environment: "test",
			},
			err: "must provide release",
		},
		{
			name: "No environment",
			conf: &Config{
				Level:       log.LevelInfo,
				Host:        "foo.com",
				LogglyToken: "xxx",
				SentryDSN:   "foo",
				Release:     "1.2.3",
			},
			err: "must provide environment",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			log, err := New(test.conf)
			testy.Error(t, test.err, err)
			re := []testy.Replacement{
				{
					Regexp:      regexp.MustCompile(`ExitFunc: \(logrus.exitFunc\) 0x[a-f0-9]+`),
					Replacement: "ExitFunc: (logrus.exitFunc) 0x000000",
				},
				{
					Regexp:      regexp.MustCompile(`\(func\(error\) error\) 0x[a-f0-9]+`),
					Replacement: `(func(error) error) 0x000000`,
				},
			}
			if d := testy.DiffInterface(testy.Snapshot(t), log, re...); d != nil {
				t.Fatal(d)
			}
		})
	}
}
