package fblogrus

import (
	"fmt"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/FlashbackSRS/log"
)

// NewFormatter returns a new formatter.
func NewFormatter() logrus.Formatter {
	return &formatter{}
}

type formatter struct {
}

var _ logrus.Formatter = &formatter{}

const timeFormat = "2006-01-02 15:04:05.0000 MST"

func (f *formatter) Format(e *logrus.Entry) ([]byte, error) {
	clone := &logrus.Entry{
		Logger:  e.Logger,
		Data:    make(map[string]interface{}, len(e.Data)),
		Time:    e.Time,
		Level:   e.Level,
		Message: e.Message,
	}
	for k, v := range e.Data {
		if k == "http_request" {
			continue
		}
		clone.Data[k] = v
	}
	lines := make([]string, 1)
	lines[0] = fmt.Sprintf("[%s%s] [%s] %s\n",
		e.Time.Format(timeFormat),
		clientTimestamp(e),
		strings.ToUpper(e.Level.String()),
		strings.TrimSuffix(e.Message, "\n"),
	)
	if err, ok := clone.Data["error"].(error); ok {
		if stack := log.StackTrace(err); stack != nil {
			for _, f := range stack {
				lines = append(lines, fmt.Sprintf("%+s:%d\n", f, f))
			}
		}
	}
	return []byte(strings.Join(lines, "")), nil
}

func clientTimestamp(e *logrus.Entry) string {
	for k, v := range e.Data {
		if k == "clientTimestamp" {
			if ts, ok := v.(time.Time); ok {
				return "/" + ts.Format(timeFormat)
			}
			break
		}
	}
	return ""
}
