// Package log provides an abstraction layer around logrus
package log

import (
	"fmt"
	"strings"
)

type (
	// Fields is an alias to logrus.Fields
	Fields map[string]interface{}
	// Level is an alias to logrus.Level
	Level uint32
)

// Logger is a logging interface
type Logger interface {
	Debugf(format string, args ...interface{})
	Printf(format string, args ...interface{})
	Errorf(format string, args ...interface{})
	Log(level Level, message string)
	// Flush should be called on exit, to ensure all logs are sent.
	Flush() error
	WithFields(Fields) Logger
	WithError(error) Logger
	Err(error)
	IsLevelEnabled(Level) bool
}

func (l Level) String() string {
	if b, err := l.MarshalText(); err == nil {
		return string(b)
	}
	return "unknown"
}

// ParseLevel takes a string level and returns the Logrus log level constant.
func ParseLevel(level string) (Level, error) {
	switch strings.ToLower(level) {
	case "error":
		return LevelError, nil
	case "info":
		return LevelInfo, nil
	case "debug":
		return LevelDebug, nil
	}

	var l Level
	return l, fmt.Errorf("not a valid log Level: %q", level)
}

// UnmarshalText implements encoding.TextUnmarshaler.
func (l *Level) UnmarshalText(text []byte) error {
	level, err := ParseLevel(string(text))
	if err != nil {
		return err
	}

	*l = level

	return nil
}

// MarshalText satisfies the encoding.TextMarshaler interface.
func (l Level) MarshalText() ([]byte, error) {
	switch l {
	case LevelDebug:
		return []byte("debug"), nil
	case LevelInfo:
		return []byte("info"), nil
	case LevelError:
		return []byte("error"), nil
	}

	return nil, fmt.Errorf("not a valid log level %d", l)
}
