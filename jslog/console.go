// +build js

package jslog

import (
	"fmt"
	"strings"
	"time"

	"github.com/gopherjs/gopherjs/js"

	"gitlab.com/FlashbackSRS/log"
)

var c = js.Global.Get("console")

// skipKeys is a list of field keys to skip logging to the console.
var skipKeys = map[string]struct{}{
	"release":     struct{}{},
	"gitBranch":   struct{}{},
	"goVersion":   struct{}{},
	"buildTime":   struct{}{},
	"environment": struct{}{},
}

const (
	defaultColor = `color: black;`
	keyColor     = `font-weight: bold`
)

func (l *logger) console(level log.Level, t time.Time, msg string) {
	var labelColor, textColor string
	switch level {
	case log.LevelError:
		labelColor = `background: red; color: white`
		textColor = `color: black;`
	case log.LevelInfo:
		labelColor = `color: black`
		textColor = `color: black`
	case log.LevelDebug:
		labelColor = `color: black`
		textColor = `color: grey`
	}
	c.Call("log", "%c"+t.Format(timeFormat)+" [%c"+strings.ToUpper(level.String())+"%c] %c"+msg,
		defaultColor, labelColor, defaultColor, textColor)
	for k, v := range l.getFields() {
		if _, skip := skipKeys[k]; !skip {
			vStr := fmt.Sprintf("%v", v)
			c.Call("log", "\t%c"+k+"%c = "+strings.ReplaceAll(vStr, "\n", "\n\t\t"),
				keyColor, textColor)
		}
	}
}
