package jslog

import (
	"fmt"
	"net/http"

	"gitlab.com/FlashbackSRS/log"
)

const timeFormat = "2006-01-02 15:04:05.999999999"

type logger struct {
	level         log.Level
	fields        log.Fields
	parent        *logger
	proxyLevel    log.Level
	client        *http.Client
	proxyEndpoint string
}

var _ log.Logger = &logger{}

func (l *logger) getFields() log.Fields {
	fields := make(log.Fields)
	for p := l; p != nil; p = p.parent {
		for k, v := range p.fields {
			if _, ok := fields[k]; !ok {
				fields[k] = v
			}
		}
	}
	return fields
}

func (l *logger) getClient() *http.Client {
	if l.client == nil && l.parent != nil {
		return l.parent.getClient()
	}
	return l.client
}

func (l *logger) getLevel() log.Level {
	if l.level == 0 && l.parent != nil {
		return l.parent.getLevel()
	}
	return l.level
}

func (l *logger) getProxyEndpoint() string {
	if l.proxyEndpoint == "" && l.parent != nil {
		return l.parent.getProxyEndpoint()
	}
	return l.proxyEndpoint
}

func (l *logger) getProxyLevel() log.Level {
	if l.proxyLevel == 0 && l.parent != nil {
		return l.parent.getProxyLevel()
	}
	return l.proxyLevel
}

// Config is a s list of configurable logging values.
type Config struct {
	// Level configures the minimum level to be logged to the console.
	Level  log.Level
	Fields log.Fields
	// Client is the HTTP client to use. Defaults to http.DefaultClient
	Client *http.Client
	// ProxyLevel configures the minimum level to be logged to the proxy server.
	ProxyLevel log.Level
	// ProxyEndpoint is the endpoint where proxied logs should be sent.
	ProxyEndpoint string
}

func New(conf *Config) log.Logger {
	client := conf.Client
	if client == nil {
		client = &http.Client{}
	}
	l := &logger{
		level:         conf.Level,
		fields:        conf.Fields,
		client:        client,
		proxyLevel:    conf.ProxyLevel,
		proxyEndpoint: conf.ProxyEndpoint,
	}
	if l.proxyLevel == 0 || l.proxyEndpoint == "" {
		l.proxyLevel = 0
		l.Printf("Logging proxy disabled by configuration")
	} else {
		l.Debugf("Proxying logs to %s", l.proxyEndpoint)
	}
	return l
}

func (l *logger) Log(level log.Level, msg string) {
	t := now()
	if level <= l.getLevel() {
		l.console(level, t, msg)
	}
	if level <= l.getProxyLevel() {
		go l.Proxy(level, t, msg)
	}
}

func (l *logger) log(level log.Level, format string, args ...interface{}) {
	l.Log(level, fmt.Sprintf(format, args...))
}

func (l *logger) Debugf(format string, args ...interface{}) {
	l.log(log.LevelDebug, format, args...)
}

func (l *logger) Printf(format string, args ...interface{}) {
	l.log(log.LevelInfo, format, args...)
}

func (l *logger) Errorf(format string, args ...interface{}) {
	l.log(log.LevelError, format, args...)
}

func (l *logger) Err(err error) {
	l.Errorf("%s", err)
}

func (l *logger) Flush() error {
	return nil
}

func (l *logger) IsLevelEnabled(level log.Level) bool {
	return level <= l.level || level <= l.proxyLevel
}

func (l *logger) WithFields(fields log.Fields) log.Logger {
	return &logger{
		fields: fields,
		parent: l,
	}
}

func (l *logger) WithError(err error) log.Logger {
	return l.WithFields(log.Fields{
		log.FieldError: err,
	})
}
