// +build !js

package jslog

import (
	"fmt"
	"strings"
	"time"

	"gitlab.com/FlashbackSRS/log"
)

func (l *logger) console(level log.Level, t time.Time, msg string) {
	fmt.Printf("%s [%s] %s\n", t.Format(timeFormat), strings.ToUpper(level.String()), msg)
}
