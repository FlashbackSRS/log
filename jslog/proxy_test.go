// +build !js

package jslog

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"gitlab.com/flimzy/testy"

	"gitlab.com/FlashbackSRS/log"
)

func init() {
	now = func() time.Time {
		t, _ := time.Parse(time.RFC3339, "2019-01-01T01:02:03Z")
		return t
	}
}

func TestProxy(t *testing.T) {
	type tst struct {
		xform func(log.Logger) log.Logger
		level log.Level
		msg   string
		err   string
	}
	tests := testy.NewTable()
	tests.Add("empty", tst{
		level: log.LevelInfo,
		msg:   "test",
	})
	tests.Add("multiple fields", tst{
		xform: func(l log.Logger) log.Logger {
			l = l.WithFields(log.Fields{
				"foo": "bar",
				"bar": 123,
			})
			l = l.WithFields(log.Fields{
				"bar": "precidence",
			})
			return l
		},
		level: log.LevelInfo,
		msg:   "foo",
	})
	tests.Add("unmarshalable", tst{
		level: log.LevelInfo,
		xform: func(l log.Logger) log.Logger {
			l = l.WithFields(log.Fields{
				"foo": make(chan int),
			})
			return l
		},
		msg: "test",
		err: "json: unsupported type: chan int",
	})

	tests.Run(t, func(t *testing.T, test tst) {
		s := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// Drop random port, for normalized test output
			r.Host = (strings.Split(r.Host, ":"))[0]
			if d := testy.DiffHTTPRequest(testy.Snapshot(t), r); d != nil {
				t.Error(d)
			}
			w.Header().Set("Content-Type", "application/json")
			_, _ = w.Write([]byte(`{"ok":true}`))
		}))
		l := New(&Config{
			ProxyLevel:    log.LevelInfo,
			ProxyEndpoint: s.URL,
		})
		if x := test.xform; x != nil {
			l = x(l)
		}
		err := l.(*logger).proxy(test.level, now(), test.msg)
		testy.Error(t, test.err, err)
	})
}
