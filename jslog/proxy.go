package jslog

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/FlashbackSRS/log"
)

var now = time.Now

// Proxy sends the log message to the central server, if level >= ProxyLevel
func (l *logger) Proxy(level log.Level, t time.Time, msg string) {
	if err := l.proxy(level, t, msg); err != nil {
		l.console(log.LevelError, now(), "failed to proxy log: "+err.Error())
	}
}

func (l *logger) proxy(level log.Level, t time.Time, msg string) error {
	e := log.Entry{
		Level:   level,
		Time:    t,
		Message: msg,
		Data:    l.getFields(),
	}
	data, err := json.Marshal(e)
	if err != nil {
		return err
	}
	req, err := http.NewRequest(http.MethodPost, l.getProxyEndpoint(), bytes.NewReader(data))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	res, err := l.getClient().Do(req)
	if err != nil {
		return err
	}
	if res.StatusCode != http.StatusOK {
		return fmt.Errorf("Unexpected status: %s", res.Status)
	}
	return nil
}
