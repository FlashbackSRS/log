package log

import (
	"github.com/pkg/errors"
	"golang.org/x/xerrors"
)

type fieldsError struct {
	error
	fields Fields
}

func (e *fieldsError) Unwrap() error {
	return e.error
}

type causer interface {
	Cause() error
}

// walkErr calls fn for every wrapped error in err, until fn returns false.
func walkErr(err error, fn func(error) bool) {
	if !fn(err) {
		return
	}
	if uw := xerrors.Unwrap(err); uw != nil {
		walkErr(uw, fn)
		return
	}
	if c, ok := err.(causer); ok {
		walkErr(c.Cause(), fn)
		return
	}
}

// ErrFields iterates through all errors, extracting any log fields.
// In the case of duplicate fields, those added later take precident.
func ErrFields(err error) Fields {
	if err == nil {
		return nil
	}
	fields := make(Fields)
	ferr := new(fieldsError)
	walkErr(err, func(err error) bool {
		if xerrors.As(err, &ferr) {
			for k, v := range ferr.fields {
				if _, ok := fields[k]; !ok {
					fields[k] = v
				}
			}
		}
		return true
	})
	if len(fields) == 0 {
		return nil
	}
	return fields
}

// ErrWithFields bundles logging context with an error.
func ErrWithFields(err error, fields Fields) error {
	return &fieldsError{error: err, fields: fields}
}

type stackTracer interface {
	StackTrace() errors.StackTrace
}

// StackTrace extracts the highest-level stack trace from err.
func StackTrace(err error) errors.StackTrace {
	var stack errors.StackTrace
	var stacker stackTracer
	walkErr(err, func(err error) bool {
		if xerrors.As(err, &stacker) {
			stack = stacker.StackTrace()
			return false
		}
		return true
	})
	return stack
}
