package log

import (
	"errors"
	"strings"
	"testing"

	pkgerrors "github.com/pkg/errors"
	"gitlab.com/flimzy/testy"
	"golang.org/x/xerrors"
)

func TestErrFields(t *testing.T) {
	type tt struct {
		err error
	}
	tests := testy.NewTable()
	tests.Add("nil", tt{nil})
	tests.Add("no fields", tt{errors.New("foo")})
	tests.Add("fields", tt{
		ErrWithFields(errors.New("foo"), Fields{"foo": "bar"}),
	})
	tests.Add("legacy-buried fields", tt{
		pkgerrors.WithStack(ErrWithFields(errors.New("foo"), Fields{"foo": "bar"})),
	})
	tests.Add("buried fields", tt{
		xerrors.Errorf("oink: %w", ErrWithFields(errors.New("foo"), Fields{"foo": "bar"})),
	})
	tests.Add("mixed fields", func(t *testing.T) interface{} {
		err := errors.New("foo")
		err = ErrWithFields(err, Fields{"foo": "bar"})
		err = xerrors.Errorf("oink: %w", err)
		err = ErrWithFields(err, Fields{"cow": "moo"})
		err = pkgerrors.WithStack(err)
		err = ErrWithFields(err, Fields{"foo": "baz"})

		return tt{err}
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		result := ErrFields(tt.err)
		if d := testy.DiffInterface(testy.Snapshot(t), result); d != nil {
			t.Error(d)
		}
	})
}

func TestWalkErr(t *testing.T) {
	t.Run("walk legacy", func(t *testing.T) {
		err := errors.New("foo")
		err = pkgerrors.Wrapf(err, "failure")

		var results []string
		walkErr(err, func(err error) bool {
			results = append(results, err.Error())
			return true
		})
		if d := testy.DiffInterface(testy.Snapshot(t), results); d != nil {
			t.Error(d)
		}
	})

	t.Run("walk wrapped errors", func(t *testing.T) {
		err := errors.New("foo")
		err = xerrors.Errorf("failure: %w", err)

		var results []string
		walkErr(err, func(err error) bool {
			results = append(results, err.Error())
			return true
		})
		if d := testy.DiffInterface(testy.Snapshot(t), results); d != nil {
			t.Error(d)
		}
	})

	t.Run("walk mixed", func(t *testing.T) {
		err := errors.New("foo")
		err = xerrors.Errorf("failure: %w", err)
		err = pkgerrors.Wrapf(err, "oh no")
		err = xerrors.Errorf("deep failure: %w", err)

		var results []string
		walkErr(err, func(err error) bool {
			results = append(results, err.Error())
			return true
		})
		if d := testy.DiffInterface(testy.Snapshot(t), results); d != nil {
			t.Error(d)
		}
	})

	t.Run("early exit", func(t *testing.T) {
		err := errors.New("foo")
		err = xerrors.Errorf("failure: %w", err)
		err = pkgerrors.Wrapf(err, "oh no")
		err = xerrors.Errorf("deep failure: %w", err)

		var results []string
		walkErr(err, func(err error) bool {
			results = append(results, err.Error())
			return !strings.HasPrefix(err.Error(), "oh no")
		})
		if d := testy.DiffInterface(testy.Snapshot(t), results); d != nil {
			t.Error(d)
		}
	})
}
