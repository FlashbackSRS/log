// +build !js

package log

import (
	"testing"

	"github.com/sirupsen/logrus"
)

func TestLevel(t *testing.T) {
	equivs := map[Level]logrus.Level{
		LevelError: logrus.ErrorLevel,
		LevelInfo:  logrus.InfoLevel,
		LevelDebug: logrus.DebugLevel,
	}
	for mine, theirs := range equivs {
		if mine != Level(theirs) {
			t.Errorf("mine (%s/%d) != theirs (%s/%d)",
				mine, mine, theirs, theirs)
		}
	}
}
