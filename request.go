package log

import (
	"net/http"
	"strconv"
	"strings"
	"time"

	"gitlab.com/flimzy/ale"
)

type request struct {
	RemoteAddr string `json:"remoteAddr"`
	Method     string `json:"method"`
	URI        string `json:"URI"`
	Status     int    `json:"status"`
	Size       int64  `json:"size"`
	UserAgent  string `json:"userAgent"`
	Elapsed    ms     `json:"elapsedMilli"`
	Location   string `json:"location,omitempty"`
	AuthUser   string `json:"authUser,omitempty"`
}

type ms int64

func (m ms) MarshalJSON() ([]byte, error) {
	r := strconv.FormatFloat(float64(m)/float64(time.Millisecond), 'f', -1, 64)
	return []byte(r), nil
}

func parseIP(addr string) string {
	if ip := addr; ip != "" {
		return ip[0:strings.LastIndex(ip, ":")]
	}
	return "<unknown>"
}

// Request logs r to lg.
func Request(lg Logger, r *ale.LogContext) {
	req := &request{
		RemoteAddr: parseIP(r.Request.RemoteAddr),
		Method:     r.Request.Method,
		URI:        r.Request.RequestURI,
		UserAgent:  r.Request.Header.Get("User-Agent"),
		Location:   r.ResponseHeader.Get("Location"),
		Status:     r.StatusCode,
		Size:       r.ResponseBytes,
		Elapsed:    ms(r.ElapsedTime),
		AuthUser:   r.AuthUser,
	}
	lg = lg.WithFields(Fields{
		"httpRequest": req,
	})
	logFunc := lg.Printf
	var redir string
	switch req.Status {
	case http.StatusRequestTimeout, http.StatusInternalServerError, http.StatusBadGateway, http.StatusGatewayTimeout, http.StatusInsufficientStorage, http.StatusLoopDetected:
		logFunc = lg.Errorf
	case http.StatusMovedPermanently, http.StatusFound, http.StatusSeeOther, http.StatusTemporaryRedirect, http.StatusPermanentRedirect:
		redir = " -> " + req.Location
	}
	authUser := req.AuthUser
	if authUser == "" {
		authUser = "-"
	}
	logFunc("%s - %s %s %s %d (%d)%s\n", req.RemoteAddr, authUser, req.Method, req.URI, req.Status, req.Size, redir)
}
