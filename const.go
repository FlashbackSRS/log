package log

// The following log levels are supported. This list is a subset of the levels
// supported by Logrus.
const (
	// LevelError level. Logs. Used for errors that should definitely be noted.
	// Commonly used for hooks to send errors to an error tracking service.
	LevelError Level = 2
	// InfoLevel level. General operational entries about what's going on inside the
	// application.
	LevelInfo Level = 4
	// DebugLevel level. Usually only enabled when debugging. Very verbose logging.
	LevelDebug Level = 5
)

// Fields with special meaning.
const (
	FieldError       = "error"
	FieldStack       = "stack"
	FieldProxyTime   = "proxyTime"
	FieldHTTPRequest = "http_request"
	FieldUserID      = "user_id"
	FieldRelease     = "release"
	FieldEnvironment = "environment"
)
