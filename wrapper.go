package log

// Wrapper wraps an underlying logger, with some convenience functions.
type Wrapper struct {
	Logger
}

var _ Logger = &Wrapper{}

// New returns a new log wrapper.
func New(lg Logger) *Wrapper {
	return &Wrapper{lg}
}
