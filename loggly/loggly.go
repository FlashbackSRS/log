// Package loggly is a wrapper around github.com/sebest/logrusly, to provide
// for custom formatting of stack traces.
package loggly

import (
	"fmt"
	"regexp"

	"github.com/pkg/errors"
	"github.com/sebest/logrusly"
	"github.com/sirupsen/logrus"
)

// Field constants duplicated to avoid import loop
const (
	FieldStack       = "stack"
	FieldHTTPRequest = "http_request"
	FieldUserID      = "user_id"
)

// Hook wraps a logrusly.LogglyHook
type Hook struct {
	*logrusly.LogglyHook
	stackTraces bool
	fallback    func(error) error
}

// NewHook returns a new Loggly hook to be added to an instance of
// log.Logger.
func NewHook(token, host string, level logrus.Level, enableStackTraces bool, tags ...string) *Hook {
	hook := logrusly.NewLogglyHook(token, host, level, tags...)
	return &Hook{
		LogglyHook:  hook,
		stackTraces: enableStackTraces,
	}
}

// Fallback sets the fallback logger, used when logging to Loggly fails.
// In case of logginf failure, fn will receive the error, and should
// handle it. If it cannot be handled, it should be returned again,
// which will cause it to be treated by the default logrus behavior.
func (h *Hook) Fallback(fn func(error) error) {
	h.fallback = fn
}

// Fire wraps the normal Fire method, but does some data transforms first:
//
// - Formats stack traces
// - Removes the full http request as it is redundant for loggly
// - Removes user name, as it is redundant with the UserCtx field
func (h *Hook) Fire(entry *logrus.Entry) error {
	if _, ok := entry.Data["skip:loggly"].(bool); ok {
		return nil
	}
	// Make a copy, so changes to Data won't propagate to other hooks
	e := new(logrus.Entry)
	*e = *entry
	e.Data = make(logrus.Fields, len(entry.Data))
	for k, v := range entry.Data {
		switch k {
		case FieldStack:
			if !h.stackTraces {
				continue
			}
			v = jsonStack(v.(errors.StackTrace))
		case FieldHTTPRequest, FieldUserID:
			continue
		}
		e.Data[k] = v
	}
	return h.fire(e)
}

func (h *Hook) fire(entry *logrus.Entry) error {
	err := h.LogglyHook.Fire(entry)
	if err != nil && h.fallback != nil {
		err = h.fallback(err)
	}
	return err
}

var spaceRE = regexp.MustCompile("[\r\n\t]+")

func jsonStack(s errors.StackTrace) []string {
	result := make([]string, 0, len(s))
	for _, frame := range s {
		result = append(result,
			spaceRE.ReplaceAllString(fmt.Sprintf("%+v", frame), " "))
	}
	return result
}
