// +build !js

package log

import (
	"errors"
	"fmt"
	"regexp"
	"testing"

	pkgerrors "github.com/pkg/errors"
	"gitlab.com/flimzy/testy"
)

func TestStackTrace(t *testing.T) {
	type tt struct {
		err error
	}
	tests := testy.NewTable()
	tests.Add("minimal err", tt{err: errors.New("foo")})
	tests.Add("minimal stack", tt{
		err: pkgerrors.WithStack(errors.New("foo")),
	})
	tests.Add("wrapped stack", func(t *testing.T) interface{} {
		err := errors.New("foo")
		err = pkgerrors.WithStack(err)
		err = fmt.Errorf("failure: %w", err)
		return tt{
			err: err,
		}
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		result := StackTrace(tt.err)
		re := testy.Replacement{
			Regexp:      regexp.MustCompile(`0x[0-9a-f]{6}`),
			Replacement: "0xXXXXXX",
		}
		if d := testy.DiffInterface(testy.Snapshot(t), result, re); d != nil {
			t.Error(d)
		}
	})
}
